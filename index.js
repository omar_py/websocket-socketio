const express = require('express');
const path = require('path');
const http = require('http');
const socketIo = require('socket.io');

const app = express();//se crea aplicacion express
const server = http.createServer(app);//se asocia un servidor http basado sobre la app express
const io = socketIo(server);//se asocia un servidor Socket.io sobre el servidor http

app.use(express.json());//parsear JSON enviados desde los clientes en el body de los request
app.use(express.static(path.join(__dirname, 'public')));//carpeta publica estatica
//datos en duro - al reiniciar servidor vuelven a este estado original - TODO usar una base de datos
const cotizaciones = [
  {'id':1, 'nombre':'Dolar', 'compra': 6000, 'venta': 6500},
  {'id':2, 'nombre':'Peso', 'compra': 200, 'venta': 250}
];

//el evento cuando un cliente se conecta - establece un web socket con el servidor
io.on('connection', socket => {
  //el parametro socket es la conexion - socket - estableciendose por el cliente
  //cada socket genera un nuevo ID unico para cada cliente
  console.log(`Nueva conexión con id: ${socket.id}`);
  //Debido a que la conexion es persistente, ya podemos enviar mensajes de ambas vias entre ambos lados

  //evento de desconexion
  socket.on('disconnect', () => {
    //cuando el cliente cierra la conexion, por ejemplo, al cerrar el navegador
    console.log(`Desconexión de id: ${socket.id}`);
  });
});

app.get('/cotizaciones', (req, res)=>{//listado de corizaciones
  res.status(200).json(cotizaciones);
});
app.put('/cotizaciones/:id', (req, res)=>{// PUT de cotizacion se usa para actualizar valores de una cotizacion
  const id = req.params.id;//se obtiene id desde el URL - dinamico
  for(cotizacion of cotizaciones) {//se busca en el array de cotizacion el que coincida con el ID buscado
    if(cotizacion.id == id) {//si son iguales
      cotizacion.compra = req.body.compra;//se actualizan valores en el array de cotizaciones
      cotizacion.venta = req.body.venta;

      //se emite evento socket.io
      io.sockets.emit('cambio_cotizacion', { id: cotizacion.id, compra: cotizacion.compra, venta: cotizacion.venta});
      //este evento 'cambio_cotizacion' es el que se define tanto en el servidor quien lo emite
      //como en el cliente donde se agrega como listener de este evento
      //en el lado cliente se define en:   socket.on("cambio_cotizacion", (data) => {....
      break;//salimos del ciclo al encontrar el resultado para que no itere innecesariamente
    }
  }

  res.status(200).json({'status':'ok'});//retorno generico
});
//iniciamos el servidor - notar es el servidor HTTP no la app de express que funciona por debajo
server.listen(3000, ()=>{
  console.log('Corriendo');
});
